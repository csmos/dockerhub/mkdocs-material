FROM squidfunk/mkdocs-material

RUN pip3 install mkdocs-awesome-pages-plugin \
	mkdocs-redirects \
	mkdocs-git-revision-date-localized-plugin
